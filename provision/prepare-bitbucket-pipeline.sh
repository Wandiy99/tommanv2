#!/usr/bin/env bash

DIR=$BITBUCKET_CLONE_DIR
ENVPATH="$DIR/.env"

if [ ! -f $ENVPATH ]; then
    cp "$DIR/.env.example" $ENVPATH
fi

NEWPASS=($(date +%s%N | md5sum))

sudo -u postgres psql -c "CREATE USER tomman WITH PASSWORD '$NEWPASS'"
sudo -u postgres psql -c "CREATE DATABASE tomman WITH OWNER tomman"
sudo -u postgres psql -d tomman -c "CREATE EXTENSION postgis"
sudo -u postgres psql -d tomman -c "CREATE EXTENSION ltree"

sed -i $ENVPATH \
  -e 's/^APP_ENV=.*/APP_ENV=testing/' \
  -e 's/^APP_URL=.*/APP_URL=http:\/\/localhost/' \
  -e 's/DB_DATABASE=.*/DB_DATABASE=tomman/' \
  -e 's/DB_USERNAME=.*/DB_USERNAME=tomman/' \
  -e "s/DB_PASSWORD=.*/DB_PASSWORD=$NEWPASS/"

