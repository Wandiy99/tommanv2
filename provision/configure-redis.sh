#!/usr/bin/env bash

sudo sed -i /etc/redis/redis.conf \
  -e 's/^save .*/\# \0/' \
  -e '/# maxmemory .*/a maxmemory 16gb' \
  -e '/# maxmemory-policy .*/a maxmemory-policy allkeys-lru' \

sudo service redis-server restart
