<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Service\Auth\Authorization;

class ServiceAuthorizationValidPermissionTest extends TestCase
{
    public function testSingleLineValidModuleInvalidPermission()
    {
        $input = 'mcore.*: notexists';
        $output = Authorization::isValidPermission($input);

        $this->assertFalse($output);
    }

    public function testSingleLineInvalidModuleValidPermission()
    {
        $input = ': read';
        $output = Authorization::isValidPermission($input);

        $this->assertFalse($output);
    }

    public function testSingleLineInvalidModuleInvalidPermission()
    {
        $input = '9.*: notexists';
        $output = Authorization::isValidPermission($input);

        $this->assertFalse($output);
    }

    public function testSingleLineValid()
    {
        $input = 'mcore.*: read';
        $output = Authorization::isValidPermission($input);

        $this->assertTrue($output);
    }

    public function testMultiLineInvalidModuleValidPermission()
    {
        $input = '
            : read
            mcore.*: read
        ';

        $output = Authorization::isValidPermission($input);

        $this->assertFalse($output);
    }

    public function testMultiLineValidModuleInvalidPermission()
    {
        $input = '
            auth.user: invalid
            mcore.*: read
        ';

        $output = Authorization::isValidPermission($input);

        $this->assertFalse($output);
    }

    public function testMultiLineValid()
    {
        $input = '
            mencorot.* : read
            mancing.gembok: read
            mancing.work: write
            auth.workzone : read_data, read_audit
        ';

        $output = Authorization::isValidPermission($input);

        $this->assertTrue($output);
    }
}
