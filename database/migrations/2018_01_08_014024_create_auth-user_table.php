<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateAuthUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * user.type = [
         * -1 => UNKNOWN
         *  0 => Newly Registered
         *  1 => Local User
         *  2 => TA
         *  3 => Mitra
         *  4 => TELKOM
         * ]
         *
         * user.timezone = [
         *  0 => UTC+7 Asia/Jakarta
         *  1 => UTC+8 Asia/Makassar
         *  2 => UTC+9 Asia/Jayapura
         * ]
         */
        DB::statement("
            CREATE TABLE auth.user(
              id                SERIAL PRIMARY KEY,
              workzone_id       SMALLINT REFERENCES auth.workzone(id),
              permission_id     SMALLINT REFERENCES auth.permission(id),
              is_login_enabled  BOOLEAN DEFAULT TRUE,
              timezone          SMALLINT NOT NULL DEFAULT 0,
              type              SMALLINT NOT NULL DEFAULT 0,
              login             text NOT NULL UNIQUE CHECK (login <> ''),
              nama              text NOT NULL CHECK (nama <> ''),
              hash              text,
              sso_cookie        text,
              remember_token    text,
              permission        text
            )
        ");

        $hash = Hash::make('telkomaksestommanmaster');
        DB::statement("
            INSERT INTO 
              auth.user(type, workzone_id, login, nama, hash, permission)
              VALUES(1, 1, 'master', 'App Master', '{$hash}', '*:FULL')
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE auth.user");
    }
}
