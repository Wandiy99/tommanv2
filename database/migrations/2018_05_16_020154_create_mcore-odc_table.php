<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreOdcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.odc(
              id BIGSERIAL PRIMARY KEY,
              workzone_id SMALLINT REFERENCES auth.workzone(id),
              label TEXT NOT NULL CHECK (label <> ''),
              panelcount SMALLINT,
              portperpanel SMALLINT DEFAULT 12,
              type SMALLINT DEFAULT 0,
              coordinate GEOMETRY(POINT, 4326)
            )
        ");

        DB::statement("CREATE INDEX ON mcore.odc(workzone_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.odc');
    }
}
