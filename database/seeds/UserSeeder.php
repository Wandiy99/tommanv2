<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Service\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'type' => 1,
            'workzone_id' => 2,
            'permission_id' => 1,
            'login' => 'meow',
            'hash' => Hash::make('meow'),
            'nama' => 'Ambassador Meow'
        ]);

        User::create([
            'type' => 1,
            'workzone_id' => 2,
            'permission_id' => 3,
            'login' => 'local',
            'hash' => Hash::make('local'),
            'nama' => 'Local User Test',
            'permission' => 'auth.workzone: read'
        ]);


        User::create([
            'workzone_id' => 2,
            'permission_id' => 2,
            'login' => '000000',
            'nama' => 'User Test 0'
        ]);

        User::create([
            'workzone_id' => 3,
            'permission_id' => 3,
            'login' => '000001',
            'nama' => 'User Test 1'
        ]);
        User::create([
            'workzone_id' => 11,
            'permission_id' => 3,
            'type' => 2,
            'login' => '000002',
            'nama' => 'User Test 2'
        ]);
    }
}
