<script>
  window.route.viewModel = (() => {
    const c = window.McoreMap.const;

    let highlightMarker;
    const showHighlightMarker = latlng => {
      if (!highlightMarker) {
        highlightMarker = new google.maps.Marker({
          map: window.map,
          position: latlng,
          icon: '/img/mcore/blue_MarkerO.png',
          zIndex: Date.now()
        });
        highlightMarker.addListener('click', () => {
          hideHighlightMarker();
          vm.setHighlight(vm.currentHighlight);
        });
      } else {
        highlightMarker.setPosition(latlng);
        highlightMarker.setVisible(true);
      }

      window.map.panTo(latlng);
    };

    const hideHighlightMarker = () => {
      if (!highlightMarker) return;

      highlightMarker.setVisible(false);
    };

    let polyline;
    const initPolyline = () => {
      if (polyline || !window.map) return;

      polyline = window.route.polyline.construct(window.map);
    };

    const addPoint = data => {
      const exists = vm.path.find(i => i.deviceType === data.deviceType && i.id === data.id);
      if (exists) {
        vm.setHighlight(exists);
        return;
      }

      Vue.set(data, 'isHighlight', false);
      vm.path.push(data);

      data.lat = Number(data.lat);
      data.lng = Number(data.lng);

      const latLng = new google.maps.LatLng(data.lat, data.lng);
      polyline.addPoint(latLng);
    };

    const removePointByIndex = index => {
      vm.path.splice(index, 1);
      polyline.removePointByIndex(index);
    };

    const movePoint = (oldIndex, newIndex) => {
      const item = vm.path.splice(oldIndex, 1)[0];
      vm.path.splice(newIndex, 0, item);

      polyline.movePoint(oldIndex, newIndex);
    };

    const getPath = () => {
      let lines = [];
      vm.path.forEach(point => {
        delete point.isHighlight;
        lines.push(point);
      });

      return lines;
    };

    let vm;
    const initVm = () => {
      vm = new Vue({
        el: '#point-view',
        data: {
          isEditing: false,
          currentHighlight: null,
          path: []
        },
        methods: {
          beginEdit: () => {
            vm.isEditing = true;
            initPolyline();
            initSortable();
          },
          setHighlight: item => {
            if (item.isHighlight) {
              item.isHighlight = false;
              vm.currentHighlight = null;

              hideHighlightMarker();

              return;
            }

            if (vm.currentHighlight) {
              vm.currentHighlight.isHighlight = false;
            }

            vm.currentHighlight = item;
            item.isHighlight = true;

            showHighlightMarker({
              lat: item.lat,
              lng: item.lng
            });
          },
          remove: index => removePointByIndex(index),

          isSto: type => type === c.types.sto,
          isOdc: type => type === c.types.odc,
          isOdp: type => type === c.types.odp,
          isPelanggan: type => type === c.types.pelanggan,
          isAlpro: type => type === c.types.alpro
        }
      });
    };

    const markerClickHandler = marker => {
      if (!vm.isEditing) return;

      addPoint(marker.data);
    };

    const loadExistingPolyline = () => {
      const path = window.route.path;
      if (!path) return;

      initPolyline();
      const bounds = new google.maps.LatLngBounds();
      path.forEach(p => {
        addPoint(p);
        bounds.extend({
          lat: Number(p.lat),
          lng: Number(p.lng)
        });
      });
      window.map.fitBounds(bounds);
    };

    const initSortable = () => {
      const el = $('#point-view > ul')[0];
      Sortable.create(el, {
        handle: '.draghandler',
        onEnd: event => movePoint(event.oldIndex, event.newIndex)
      });
    };

    const init = () => {
      window.route.map.init();
      window.Alpro.grid.init(markerClickHandler);
      window.McoreMap.features.sto.init(markerClickHandler);
      window.McoreMap.features.grid.init(markerClickHandler);

      initVm();
      loadExistingPolyline();
    };

    return {
      init,
      getPath
    };
  })();
</script>
