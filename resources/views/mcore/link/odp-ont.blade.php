@extends('app')

@section('title', 'Sambungan Pelanggan')

@section('body')
  @isset($linkData->src_val)
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/odp/workzone/{{ $linkData->odp_workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $linkData->odp_workzone_label }}</span>
        </a>
      </li>
      <li>
        <a href="/mcore/odp/{{ $linkData->odp_id }}">
          <span class="label label-primary">ODP</span>
          <span>{{ $linkData->odp_label }}</span>
        </a>
      </li>
      <li class="active">
        <span>PORT</span>
        <span class="label label-outline label-info">{{ $linkData->src_val }}</span>
      </li>
    </ol>
  @else
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/pelanggan/workzone/{{ $linkData->pelanggan_workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $linkData->pelanggan_workzone_label }}</span>
        </a>
      </li>
      <li>
        <a href="/mcore/pelanggan/{{ $linkData->pelanggan_id }}">
          <span class="label label-success">{{ $linkData->pelanggan_kode }}</span>
          <span>{{ $linkData->pelanggan_label }}</span>
        </a>
      </li>
      <li class="active">
        <span>Sambungan</span>
      </li>
    </ol>
  @endisset

  <div class="page-header">
    <h1>
      <i class="fas fa-share-alt"></i>
      <span>Sambungan Pelanggan</span>
    </h1>
  </div>

  <form id="form" class="panel" method="post">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        @isset($linkData->odp_id)
          <div class="col-md-3">
            <label>ODP</label>
            <div>
              <input name="src_id" value="{{ $linkData->odp_id }}" type="hidden">
              <a href="/mcore/odp/{{ $linkData->odp_id }}">{{ $linkData->odp_label }}</a>
            </div>
          </div>
          <div class="col-md-1">
            <label>Port</label>
            <div>
              <input name="src_val" value="{{ $linkData->src_val }}" type="hidden">
              <span>{{ $linkData->src_val }}</span>
            </div>
          </div>
        @else
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-12">
                @include('partial.form.select.keyval', [
                  'label' => 'ODP',
                  'object' => $linkData,
                  'field' => 'src_id',
                  'options' => [],
                  'canEdit' => true,
                  'attributes' => [
                    'required' => true,
                    'data-msg-required' => 'Silahkan pilih ODP'
                  ]
                ])
              </div>

              <div class="col-md-12">
                @include('partial.form.select.val', [
                  'label' => 'Port',
                  'object' => $linkData,
                  'field' => 'src_val',
                  'options' => [],
                  'canEdit' => true,
                  'attributes' => [
                    'required' => true,
                    'data-msg-required' => 'Silahkan pilih Port ODP'
                  ]
                ])
                <i id="port-loading" class="fas fa-spinner fa-pulse" style="display:none"></i>
              </div>
            </div>
          </div>
        @endisset

        <div class="col-md-4">
          @include('partial.form.text', [
            'label' => 'Label Kabel',
            'object' => $linkData,
            'field' => 'med_val',
            'canEdit' => $canEdit,
            'help' => 'Misal: Drop Core 100m'
          ])
        </div>

        <div class="col-md-4">
          @isset($linkData->pelanggan_id)
            <label>Pelanggan</label>
            <div>
              <input name="dst_id" value="{{ $linkData->pelanggan_id }}" type="hidden">
              <span class="label label-primary">{{ $linkData->pelanggan_kode }}</span>
              <a href="/mcore/pelanggan/{{ $linkData->pelanggan_id }}">{{ $linkData->pelanggan_label }}</a>
            </div>
          @else
            @include('partial.form.select.val', [
              'label' => 'Pelanggan',
              'object' => $linkData,
              'field' => 'dst_id',
              'options' => [],
              'canEdit' => $canEdit,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih Pelanggan'
              ]
            ])
          @endif
        </div>
      </div>
    </div>

    @if ($canEdit)
      <div class="panel-footer text-right">
        @if (isset($linkData->src_id) && isset($linkData->dst_id))
          <button name="action" value="remove" type="submit" class="btn btn-warning pull-left">
            <i class="fas fa-unlink"></i>
            <span>Cabut Sambungan</span>
          </button>
        @endif

        <button name="action" value="save" type="submit" class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    @endif
  </form>
@endsection

@section('script')
  <script>
    @isset($linkData->odp_id)
      $(function () {
        $pelangganInput = $('#input-dst_id');

        $pelangganInput.select2({
          ajax: {
            url: '/mcore/pelanggan/link/workzone/{{ $linkData->odp_workzone_id }}.select2',
            data: params => {
              return {
                q: params.term,
                page: params.page || 1
              }
            }
          },
          placeholder: '...',
          escapeMarkup: m => m
        });
      });
    @else
      $(function () {
        $odpInput = $('#input-src_id');
        $odpPortInput = $('#input-src_val');
        $odpPortLoadingIndicator = $('#port-loading');

        $odpInput.select2({
          ajax: {
            url: '/mcore/odp/workzone/{{ $linkData->pelanggan_workzone_id }}.select2',
            data: params => {
              return {
                q: params.term,
                page: params.page || 1
              }
            }
          },
          placeholder: '...',
          escapeMarkup: m => m
        });

        $odpInput.on('change', event => {
          if ($odpPortInput.hasClass('select2-hidden-accessible')) {
            $odpPortInput.select2('destroy').empty();
          }

          $odpPortLoadingIndicator.show();
          $odpPortInput.hide();

          $.ajax('/mcore/odp/' + $odpInput.val() + '/ports.select2')
            .then(response => {
              $odpPortLoadingIndicator.hide();
              $odpPortInput.show().select2({
                data: response.results,
                placeholder: '...',
                escapeMarkup: m => m
              });
            });
        });
      });
    @endif
  </script>

  @include('partial.form.validate', ['id' => 'form'])
@endsection
