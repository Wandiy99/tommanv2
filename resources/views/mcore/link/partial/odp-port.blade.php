<span class="{{ $link ? 'text-muted' : '' }}">{{ $portNum }}</span>

@isset($link->pelanggan_id)
  <i class="fas fa-long-arrow-alt-right text-success"></i>

  <span class="label label-outline label-default">{{ $link->pelanggan_kode }}</span>
  <span>{{ $link->pelanggan_label }}</span>
@endisset
