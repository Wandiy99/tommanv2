@extends('app')

<?php if (!isset($pelangganData)) $pelangganData = null ?>
<?php $title = $pelangganData->label ?? 'Input Pelanggan' ?>

@section('title', $title)

@section('style')
  <style>
    @media (min-width: 768px) {
      #input-keterangan {
        min-height: 174px;
      }

      .panel-footer .btn-primary {
        width: 115px;
        text-align: left;
      }
    }
  </style>
@endsection

@section('body')
  @if (isset($pelangganData->id))
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/pelanggan/workzone/{{ $pelangganData->workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $pelangganData->workzone_label }}</span>
        </a>
      </li>
      <li class="active">
        <span class="label label-primary">Pelanggan</span>
        <span>{{ $pelangganData->label }}</span>
      </li>
    </ol>
  @endif

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $title }}</span>
    </h1>
  </div>

  <form id="form" class="panel" method="post">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        <div class="col-md-3">
          @include('partial.form.text', [
            'label' => 'Serial Number',
            'object' => $pelangganData,
            'field' => 'kode',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Serial Number'
            ]
          ])
        </div>

        <div class="col-md-3">
          @include('partial.form.text', [
            'label' => 'Nama',
            'object' => $pelangganData,
            'field' => 'label',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Nama Pelanggan'
            ]
          ])
        </div>

        <div class="col-md-3">
          @include('partial.form.select.keyval', [
            'label' => 'Tipe',
            'object' => $pelangganData,
            'field' => 'type',
            'options' => $types,
            'canEdit' => $canEdit
          ])
        </div>

        <div class="col-md-3">
          @include('partial.workzone.formcontrol', [
            'label' => 'Work Zone',
            'object' => $pelangganData,
            'field' => 'workzone_id',
            'displayField' => 'workzone_label',
            'canEdit' => $canEdit,
            'workzoneTree' => $workzoneTree,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan pilih Area Kerja'
            ]
          ])
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              @include('partial.form.text', [
                'label' => 'PIC',
                'object' => $pelangganData,
                'field' => 'pic',
                'canEdit' => $canEdit
              ])
            </div>

            <div class="col-md-12">
              @include('partial.form.text', [
                'label' => 'Alamat',
                'object' => $pelangganData,
                'field' => 'alamat',
                'canEdit' => $canEdit
              ])
            </div>

            <div class="col-md-12">
              @include('partial.mapmarker.formcontrol', [
                'label' => 'Koordinat (lat, lng)',
                'object' => $pelangganData,
                'field' => 'coordinate',
                'canEdit' => $canEdit
              ])
            </div>
          </div>
        </div>

        <div class="col-md-6">
          @include('partial.form.textarea', [
            'label' => 'Keterangan',
            'object' => $pelangganData,
            'field' => 'keterangan',
            'canEdit' => $canEdit,
            'help' => 'Dapat diisi catatan untuk teknisi, keterangan lokasi, kontak tambahan, dan sebagainya'
          ])
        </div>
      </div>
    </div>

    @if ($canEdit)
      <div class="panel-footer text-right">
        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    @endif
  </form>

  @isset($pelangganData->id)
    <div class="panel">
      <div class="panel-body">
        @if ($link)
          <span class="label label-primary">ODP</span>
          <a href="/mcore/odp/{{ $link->odp_id }}">{{ $link->odp_label }}</a>

          <span class="label label-info">PORT</span>
          <span>{{ $link->src_val }}</span>

          @if ($link->med_val)
            <span class="label label-default">KABEL</span>
            <span>{{ $link->med_val }}</span>
          @endif
        @endif

        <a href="/mcore/pelanggan/link/{{ $pelangganData->id }}" class="btn btn-default pull-md-right m-xs-t-20">
          <i class="fas fa-link"></i>
          <span>Sambungan</span>
        </a>
      </div>
    </div>
  @endisset
@endsection

@section('script')
  @include('partial.mapmarker.modal')
  @include('partial.mapmarker.script', ['field' => 'coordinate', 'canEdit' => $canEdit])

  @include('partial.workzone.modal')
  @include('partial.workzone.script', [
    'field' => 'workzone_id',
    'displayField' => 'workzone_label',
    'workzoneTree' => $workzoneTree,
    'canEdit' => $canEdit
  ])

  @include('partial.form.validate', ['id' => 'form'])
@endsection
