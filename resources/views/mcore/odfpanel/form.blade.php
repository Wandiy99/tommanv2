@extends('app')

<?php $title = $panelData->label ?? 'Input OTB' ?>
@section('title', $title)

@section('style')
  <?php $slotWidth = ($panelData->slotportcount ?? 1) * 52 ?>
  <style>
    .otb-slot {
      background: #374049;
      padding: 0 10px;
      margin-bottom: 10px;
      width: {{ $slotWidth }}px;
    }
    .flex-h > .portgroup {
      margin: 10px 20px 10px 0;
    }
    .flex-v > .portgroup {
      margin: 10px 0;
    }

    .portgroup {
      margin-bottom: 10px;
    }
    .port, .rearport {
      cursor: pointer;
    }
    .popover {
      max-width: 800px;
    }
    .popover .label, .popover .badge {
      margin-left: 15px;
      margin-right: 5px;
    }
  </style>
@endsection

@section('body')
  <ol class="breadcrumb page-breadcrumb">
    <li>
      <a href="/mcore/sto/workzone/{{ $stoData->workzone_id }}">
        <span class="label label-primary">WZ</span>
        <span>{{ $stoData->workzone_label }}</span>
      </a>
    </li>
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}">
        <span class="label label-primary">STO</span>
        <span>{{ $stoData->label }}</span>
      </a>
    </li>
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}">
        <span class="label label-primary">room</span>
        <span>{{ $roomData->label }}</span>
      </a>
    </li>
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/odf/{{ $odfData->id }}">
        <span class="label label-primary">ODF</span>
        <span>{{ $odfData->label }}</span>
      </a>
    </li>
    <li class="active">
      @if (isset($panelData->id))
        <span class="label label-primary">OTB</span>
        <span>{{ $panelData->label }}</span>
      @else
        Panel
      @endif
    </li>
  </ol>

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $title }}</span>
    </h1>
  </div>

  <form id="form" class="panel" method="post">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        <div class="col-md-3">
          @include('partial.form.text', [
            'label' => 'Nama/Label',
            'object' => $panelData,
            'field' => 'label',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Nama/Label OTB'
            ]
          ])
        </div>

        <div class="col-md-3">
          @include('partial.form.text', [
            'label' => 'Jumlah Slot',
            'object' => $panelData,
            'field' => 'slotcount',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Jumlah Slot',
              'type' => 'number',
              'min' => '1',
              'max' => '1000'
            ]
          ])
        </div>

        <div class="col-md-3">
          @include('partial.form.text', [
            'label' => 'Jumlah Port Masing<sup>2</sup> Slot',
            'object' => $panelData,
            'field' => 'slotportcount',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi Jumlah Port',
              'type' => 'number',
              'min' => '1',
              'max' => '1000'
            ]
          ])
        </div>

        <div class="col-md-3">
          @if ($canEdit)
            <?php $hasError = $errors->has('slotvertical') ?>
            <fieldset class="form-group form-message-light {{ $hasError?'has-error':'' }}">
              <label>Posisi Slot</label>
              <div>
                <label class="custom-control custom-radio m-r-4">
                  <input type="radio" name="slotvertical" value="1" class="custom-control-input"
                         {{ (isset($panelData) && $panelData->slotvertical) ? 'checked' : '' }}>
                  <span class="custom-control-indicator"></span>
                  <i class="fas fa-bars" style="transform: rotate(90deg)"></i>
                  <span>Vertical</span>
                </label>

                <label class="custom-control custom-radio">
                  <input type="radio" name="slotvertical" value="0" class="custom-control-input"
                         {{ (isset($panelData) && $panelData->slotvertical) ? '' : 'checked' }}>
                  <span class="custom-control-indicator"></span>
                  <i class="fas fa-bars"></i>
                  <span>Horizontal</span>
                </label>
              </div>

              @if ($hasError)
                @foreach($errors->get('slotvertical') as $errorText)
                  <small class="form-message light">{{ $errorText }}</small>
                @endforeach
              @endif
            </fieldset>
          @else
            <div class="form-group">
              <label>Posisi Slot</label>
              <p class="form-control-static">
                @if ($panelData->slotvertical)
                  <i class="fas fa-bars" style="transform: rotate(90deg)"></i>
                  <span>Vertical</span>
                @else
                  <i class="fas fa-bars"></i>
                  <span>Horizontal</span>
                @endif
              </p>
            </div>
          @endif
        </div>
      </div>
    </div>

    @if ($canEdit)
      <div class="panel-footer text-right">
        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    @endif
  </form>

  @if (isset($panelData->id))
    @include('mcore.odfpanel.panel')
    @include('mcore.odfpanel.modal')
  @endif
@endsection

@section('script')
  @include('mcore.odfpanel.script')
  @include('partial.form.validate', ['id' => 'form'])
@endsection
