<script>
  $(function() {
    const $inputCoordinate = $('#input-{{ $field }}');
    const onCoordinateSelected = (lat, lng) => {
      $inputCoordinate.val(lat + ',' + lng);
    };

    MapMarkerModal.setCallback(onCoordinateSelected);
    MapMarkerModal.setCanEdit({{ $canEdit ? 'true' : 'false' }});

    $('#btnPick-{{ $field }}').click(function() {
      let lat, lng;
      const coordinateText = $inputCoordinate.val().trim();

      if (coordinateText.length) {
        const coordinates = $inputCoordinate.val().split(',');
        lat = Number(coordinates[0].trim());
        lng = Number(coordinates[1].trim());
      }
      else {
        lat = lng = 0;
      }

      MapMarkerModal.open(lat, lng);
    })
  });
</script>
