@extends('app')

@section('title', 'Search User')

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-search"></i>
      <span>Search Users</span>
    </h1>
  </div>

  <div class="panel">
    <div class="panel-heading">
      <span class="panel-title">
        <span>Current Workzone</span>
        <span class="label label-info">{{ $currentUser->workzone_nama }}</span>
      </span>
      <div class="panel-heading-controls col-sm-6">
        <form>
          <div class="input-group input-group-sm">
            <input name="q" value="{{ $q }}" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button class="btn">
                <i class="fas fa-search"></i>
              </button>

              <a class="btn btn-link" href="/auth/user">Clear</a>
            </span>
          </div>
        </form>

      </div>
    </div>

    <div class="panel-body">
      @if (!count($list))
        <span>NOTHING FOUND</span>
      @else
        <div class="list-group">
          @foreach($list as $entry)
            <a href="#" class="list-group-item">
              <h4 class="list-group-item-heading">
                {{ $entry->login }}

                @if ($entry->type == \App\Service\Auth\User::TYPE_NEWUSER)
                  <span class="label label-primary">new user</span>
                @elseif ($entry->type == \App\Service\Auth\User::TYPE_LOCALUSER)
                  <span class="label label-info">local user</span>
                @else
                  <span class="label label-default">{{ $entry->workzone_nama }}</span>
                @endif
              </h4>
              <span>{{ $entry->nama }}</span>
            </a>
          @endforeach
        </div>
      @endif
    </div>

    @if ($list->total() > $list->perPage())
      <div class="panel-footer text-center">
        {{ $list->links() }}
      </div>
    @endif
  </div>
@endsection
