@extends('app')

@section('title', 'WorkZone')

@section('style')
  <style>
    .button-edit {
      visibility: collapse;
    }

    .workzone-tree-label:hover > .button-edit {
      visibility: visible;
    }

    .workzone-tree-editor {
      margin-right: 10px;
      display: inline-block;
    }
    .workzone-tree-editor input {
      display: inline-block;
      width: 200px;
    }
    .workzone-tree-editor i.fa {
      margin-right: 0;
    }
  </style>
@endsection

@section('body')
  <div class="page-header">
    <h1>
      <i class="fas fa-map-signs"></i>
      <span>Work Zone</span>
    </h1>
  </div>

  <div id="tree-container" class="white-box table-responsive">
    <workzone-collection :items="workzoneData">Loading...</workzone-collection>
  </div>
@endsection

@section('script')
  <script id="template-tree-item" type="text/x-template">
    <li>
      <div class="workzone-tree-label" v-show="!isEditing">
        <i class="fas fa-spinner fa-pulse" v-show="isInFlight"></i>
        <span>@{{ item.label }}</span>

        <button class="btn btn-link button-edit" v-show="!isInFlight && item.isWritable" v-on:click="startEdit">
          <i class="fas fa-pencil-alt"></i>
          <span>edit</span>
        </button>
      </div>

      <form class="workzone-tree-editor" v-show="isEditing" v-on:submit.prevent="submitEdit">
        <input type="text" class="form-control input-edit" v-model="inputNama" title="Nama">
        <button class="btn btn-info" type="submit">
          <i class="fas fa-check"></i>
        </button>
        <button class="btn-link" type="button" v-on:click="cancelEdit">
          cancel
        </button>
      </form>

      <workzone-collection :items="item.children" :parent="item"></workzone-collection>
    </li>
  </script>

  <script id="template-tree-item-collection" type="text/x-template">
    <ul class="tree">
      <workzone-item v-for="item in items" :key="item.id" :item="item"></workzone-item>

      <li v-if="!parent.isWaitingNetworkCreate && parent.isWritable">
        <div class="workzone-tree-label">
          <button class="btn-link" v-on:click="startCreate" v-show="!isCreating">
            <i class="fas fa-plus"></i>
            <span>add</span>
          </button>

          <form class="workzone-tree-editor" v-show="isCreating" v-on:submit.prevent="submitCreate">
            <input type="text" class="form-control input-create" v-model="inputNama" title="Nama">
            <button class="btn btn-info" type="submit">
              <i class="fas fa-check"></i>
            </button>
            <button class="btn-link" type="button" v-on:click="cancelCreate">
              cancel
            </button>
          </form>
        </div>
      </li>
    </ul>
  </script>

  <script>
    (function() {
      'use strict';

      const URL_CREATE = '/auth/workzone/api/create';
      const URL_RETRIEVE = '/auth/workzone/api/all';
      const URL_UPDATE = '/auth/workzone/api/update';

      Vue.component('workzone-item', {
        template: '#template-tree-item',
        props: {
          item: Object,
          isWritable: Boolean
        },
        data: () => ({
          isEditing: false,
          inputNama: ''
        }),
        computed: {
          isInFlight: function() { return (this.item.isWaitingNetworkCreate || this.item.isWaitingNetworkUpdate) }
        },
        created: function() {
          this.inputNama = this.item.label;
          this.$set(this.item, 'isWaitingNetworkUpdate', false);
        },
        methods: {
          startEdit: function() { this.isEditing = true },
          cancelEdit: function() { this.isEditing = false },
          submitEdit: function() {
            let item = this.item; // scope
            let oldName = item.label;

            let postData = {
              id: item.id,
              label: this.inputNama
            };

            this.isEditing = false;
            item.label = this.inputNama;
            item.isWaitingNetworkUpdate = true;

            jQuery
              .post(
                URL_UPDATE,
                postData,
                'json'
              )
              .done(function(data) {
                item.label = data.label;
                item.isWaitingNetworkUpdate = false;
              })
              .fail(function(jqxhr, status, err) {
                console.log(err);
                // TODO: display error
                item.label = oldName;
              });
          }
        },
        watch: {
          isEditing() {
            if (!this.isEditing) return;

            let textBox = this.$el.getElementsByClassName('input-edit')[0];
            setTimeout(() => textBox.select(), 50);
          }
        }
      });

      Vue.component('workzone-collection', {
        template: '#template-tree-item-collection',
        props: {
          parent: {
            'type': Object,
            'default': () => ({
              path: '',
              isWaitingNetworkCreate: false
            })
          },
          items: {
            'type': Array,
            'default': () => []
          }
        },
        data: () => ({
          isCreating: false,
          inputNama: ''
        }),
        methods: {
          startCreate: function() { this.isCreating = true },
          cancelCreate: function() { this.isCreating = false },
          submitCreate: function () {

            let postData = {
              label: this.inputNama,
              path: this.parent.path
            };

            let newData = {
              id: Date.now(),
              label: this.inputNama,
              path: this.parent.path,
              isWaitingNetworkCreate: true
            };
            this.items.push(newData);

            this.isCreating = false;
            this.inputNama = '';

            jQuery
              .post(
                URL_CREATE,
                postData,
                'json'
              )
              .done(data => {
                newData.id = data.id;
                newData.label = data.label;
                newData.path = data.path;
                newData.isWritable = true;
                newData.isWaitingNetworkCreate = false;
              })
              .fail((jqxhr, status, err) => {
                console.log(err);
                // TODO: display error
                this.items.pop();
              });
          }
        },
        watch: {
          isCreating() {
            if (!this.isCreating) return;
            window.el = this.$el;

            let textBoxes = this.$el.getElementsByClassName('input-create');
            let textBox   = textBoxes[textBoxes.length - 1];
            setTimeout(() => textBox.select(), 50);
          }
        }
      });

      jQuery
        .get(URL_RETRIEVE)
        .done(data => {
          new Vue({
            el: '#tree-container',
            data: {
              workzoneData: data
            }
          });
        })
        .fail((jqxhr, status, err) => {
          // TODO: display network error
          // TODO: display cache
        });

    })();
  </script>
@endsection
