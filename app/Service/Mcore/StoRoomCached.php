<?php

namespace App\Service\Mcore;

use App\Service\Cache;

class StoRoomCached
{
    const PREFIX = 'Mcore.Sto.Room:';
    const TAG_LISTING = self::PREFIX.'List';

    const KEY_BY_STO = self::PREFIX.'Sto=';
    const KEY_BY_ID = self::PREFIX.'Id=';

    public static function keyBySto($stoId)
    {
        return self::KEY_BY_STO.$stoId;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function listBySto($stoId)
    {
        $key = self::keyBySto($stoId);

        $dataSource = function () use ($stoId) {
            return StoRoom::listBySto($stoId);
        };

        return Cache::store($key, $dataSource);
    }

    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return StoRoom::getById($id);
        };

        return Cache::store($key, $dataSource);
    }

    /**
     * @param $user_id
     * @param $sto_id
     * @param $label
     * @return int
     * @throws \Throwable
     */
    public static function create($user_id, $sto_id, $label)
    {
        $id = StoRoom::create($user_id, $sto_id, $label);

        Cache::del(self::keyBySto($sto_id));

        return $id;
    }

    /**
     * @param $user_id
     * @param $room_id
     * @param $label
     * @throws \Throwable
     */
    public static function update($user_id, $room_id, $label)
    {
        StoRoom::update($user_id, $room_id, $label);

        $key = self::keyById($room_id);
        $flushKeys = [$key];

        [$data, $mtime] = Cache::get($key);
        if ($mtime !== null) {
            $flushKeys[] = self::keyBySto($data->sto_id);
        }

        Cache::flushKeys($flushKeys);
    }
}
