<?php

namespace App\Service\Mcore;

use App\Service\Cache;

class OdcSplitterCached
{
    const PREFIX = 'Mcore.OdcSplitter:';
    const KEY_BY_ODC = self::PREFIX.'Odc=';

    public static function keyByOdc($id)
    {
        return self::KEY_BY_ODC.$id;
    }

    /**
     * @param $user_id
     * @param $odc_id
     * @param $label
     * @param $odc_panel
     * @param $odc_port
     * @param $splitter_port
     * @throws \Throwable
     */
    public static function create($user_id, $odc_id, $label, $odc_panel, $odc_port, $splitter_port)
    {
        OdcSplitter::create($user_id, $odc_id, $label, $odc_panel, $odc_port, $splitter_port);

        Cache::flushTags([OdcCached::tagById($odc_id)]);
    }

    public static function getByOdc($id)
    {
        $key = self::keyByOdc($id);

        $dataSource = function () use ($id) {
            return OdcSplitter::getByOdc($id);
        };

        $tagGenerator = function () use ($id) {
            return [OdcCached::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * @param $user_id
     * @param $odc_id
     * @param $splitter_id
     * @param $label
     * @throws \Throwable
     */
    public static function update($user_id, $odc_id, $splitter_id, $label)
    {
        OdcSplitter::update($user_id, $odc_id, $splitter_id, $label);

        Cache::flushTags([OdcCached::tagById($odc_id)]);
    }
}
