<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Auth\Workzone;

class Sto
{
    private static function table()
    {
        return DB::table('mcore.sto AS sto');
    }

    private static function auditTable()
    {
        return DB::table('mcore.sto_audit AS audit');
    }

    private static function db()
    {
        return self::table()
            ->leftJoin('auth.workzone AS workzone', 'sto.workzone_id', '=', 'workzone.id')
            ->select(
                'sto.id',
                'sto.label',
                'sto.workzone_id',
                //
                'workzone.label AS workzone_label',
                'workzone.path AS workzone_path',
                //
                DB::raw('ST_X(sto.coordinate) AS lng'),
                DB::raw('ST_Y(sto.coordinate) AS lat')
            );
    }

    private static function auditDb()
    {
        return self::auditTable()
            ->leftJoin('auth.user AS user', 'user.id', '=', 'audit.user_id')
            ->select(
                'audit.id',
                //
                'audit.user_id',
                'user.login AS user_login',
                'user.nama AS user_nama',
                //
                'audit.operation',
                'audit.timestamp'
            );
    }

    public static function getById($id)
    {
        $lastOp = self::auditDb()
            ->where('sto_id', $id)
            ->orderBy('timestamp', 'DESC')
            ->limit(1)
            ->first();

        $lastOp->datetime = new \DateTime($lastOp->timestamp, new \DateTimeZone("UTC"));

        $dataSto = self::db()->where('sto.id', $id)->first();
        $dataSto->last_operation = $lastOp;

        return $dataSto;
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $query = self::db()->where('workzone.path', '<@', $path);

        if ($search) {
            $clause = 'sto.label ILIKE ?';
            $term = '%'.str_replace(' ', '%', $search).'%';

            $query->whereRaw($clause, ["%$term%"]);
        }

        return $query->paginate($limit, ['*'], 'NOTUSED', $page);
    }

    /**
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param float $lat
     * @param float $lng
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(int $user_id, int $workzone_id, string $label, float $lat, float $lng)
    {
        $id = 0;

        DB::transaction(function () use ($user_id, $label, $workzone_id, $lat, $lng, &$id) {
            $data = compact('label', 'workzone_id');
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            $id = self::table()->insertGetId($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_id, $id, 'insert', $data);
        });

        return $id;
    }

    /**
     * @param int $user_Id for history/audit
     * @param int $sto_id
     * @param int $workzone_id
     * @param string $label
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(int $user_Id, int $sto_id, int $workzone_id, string $label, float $lat, float $lng)
    {
        DB::transaction(function () use ($user_Id, $sto_id, $label, $workzone_id, $lat, $lng) {
            $data = compact('label', 'workzone_id');
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            self::table()->where('id', $sto_id)->update($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_Id, $sto_id, 'update', $data);
        });
    }

    public static function countByWorkzonePath($path)
    {
        $sql = "
            SELECT
              id,
              label,
              path,
              '/mcore/sto/workzone/' || id AS url,
              CASE 
                WHEN STO.count IS NOT NULL THEN STO.count
                ELSE 0
              END AS count
              
            FROM
              auth.workzone
              
            LEFT JOIN
              (SELECT workzone_id, COUNT(workzone_id) FROM mcore.sto GROUP BY workzone_id) AS STO
              ON STO.workzone_id = id
              
            WHERE
              path <@ ?
              
            ORDER BY
              path
        ";
        $params = [$path];
        $result = DB::select($sql, $params);

        return Workzone::grow($result);
    }

    public static function allHistoryById($id)
    {
        return self::auditDb()
            ->where('sto_id', $id)
            ->orderBy('timestamp', 'DESC')
            ->get();
    }

    public static function insertHistory($user_id, $sto_id, $operation, array $data)
    {
        $insertData = [
            'sto_id' => $sto_id,
            'user_id' => $user_id,
            'timestamp' => DB::raw("NOW() AT TIME ZONE 'utc'"),
            'operation' => $operation,
            'data' => json_encode($data)
        ];
        self::auditTable()->insert($insertData);
    }
}
