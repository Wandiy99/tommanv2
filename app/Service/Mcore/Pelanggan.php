<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Auth\Workzone;

class Pelanggan
{
    public const TYPES = [
        0 => 'Rumah',
        1 => 'Kantor',
        2 => 'wifi.id',
        3 => 'BTS Telkomsel'
    ];

    private static function table()
    {
        return DB::table('mcore.pelanggan AS pelanggan');
    }

    private static function db()
    {
        return self::table()
            ->leftJoin('auth.workzone AS workzone', 'pelanggan.workzone_id', '=', 'workzone.id')
            ->select(
                'pelanggan.id',
                'pelanggan.label',
                'pelanggan.workzone_id',
                'kode',
                'type',
                'pic',
                'alamat',
                'keterangan',
                //
                'workzone.label AS workzone_label',
                'workzone.path AS workzone_path',
                //
                DB::raw('ST_X(pelanggan.coordinate) AS lng'),
                DB::raw('ST_Y(pelanggan.coordinate) AS lat')
            );
    }

    private static function auditTable()
    {
        return DB::table('mcore.pelanggan_audit AS audit');
    }

    public static function countByWorkzonePath($path)
    {
        $sql = "
            SELECT
              id,
              label,
              path,
              '/mcore/pelanggan/workzone/' || id AS url,
              CASE 
                WHEN pelanggan.count IS NOT NULL THEN pelanggan.count
                ELSE 0
              END AS count
              
            FROM
              auth.workzone
              
            LEFT JOIN
              (SELECT workzone_id, COUNT(workzone_id) FROM mcore.pelanggan GROUP BY workzone_id) AS pelanggan
              ON pelanggan.workzone_id = id
              
            WHERE
              path <@ ?
              
            ORDER BY
              path
        ";
        $params = [$path];
        $result = DB::select($sql, $params);

        return Workzone::grow($result);
    }

    /**
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $kode
     * @param string $label
     * @param int $type
     * @param string $pic
     * @param string $alamat
     * @param string $keterangan
     * @param float $lat
     * @param float $lng
     * @return int id of newly created object
     * @throws \Throwable when database transaction failed
     */
    public static function create(
        int $user_id,
        int $workzone_id,
        string $kode,
        string $label,
        int $type,
        ?string $pic,
        ?string $alamat,
        ?string $keterangan,
        float $lat,
        float $lng
    ) {
        $id = 0;
        $data = compact('workzone_id', 'kode', 'label', 'type', 'pic', 'alamat', 'keterangan');

        DB::transaction(function () use (&$id, $data, $user_id, $lat, $lng) {
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            $id = self::table()->insertGetId($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_id, $id, 'insert', $data);
        });

        return $id;
    }

    public static function insertHistory($user_id, $pelanggan_id, $operation, array $data)
    {
        $insertData = [
            'pelanggan_id' => $pelanggan_id,
            'user_id' => $user_id,
            'timestamp' => DB::raw("NOW() AT TIME ZONE 'utc'"),
            'operation' => $operation,
            'data' => json_encode($data)
        ];
        self::auditTable()->insert($insertData);
    }

    public static function getById($id)
    {
        // TODO: lastOp

        $data = self::db()->where('pelanggan.id', $id)->first();

        return $data;
    }

    /**
     * @param int $user_id for history/audit
     * @param int $pelanggan_id
     * @param int $workzone_id
     * @param string $kode
     * @param string $label
     * @param int $type
     * @param string $pic
     * @param string $alamat
     * @param string $keterangan
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(
        int $user_id,
        int $pelanggan_id,
        int $workzone_id,
        string $kode,
        string $label,
        int $type,
        ?string $pic,
        ?string $alamat,
        ?string $keterangan,
        float $lat,
        float $lng
    ) {
        $data = compact('workzone_id', 'kode', 'label', 'type', 'pic', 'alamat', 'keterangan');
        DB::transaction(function () use ($user_id, $pelanggan_id, $data, $lat, $lng) {
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            self::table()->where('id', $pelanggan_id)->update($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_id, $pelanggan_id, 'update', $data);
        });
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $query = self::db()->where('workzone.path', '<@', $path)->orderBy('label');

        if ($search) {
            $clause = '(
                   pelanggan.label ILIKE ? 
                OR kode ILIKE ? 
                OR pic ILIKE ? 
                OR alamat ILIKE ? 
                OR keterangan ILIKE ?
            )';
            $term = '%'.str_replace(' ', '%', $search).'%';
            $param = array_fill(0, 5, "%$term%");

            $query->whereRaw($clause, $param);
        }

        return $query->paginate($limit, ['*'], 'NOTUSED', $page);
    }
}
