<?php

namespace App\Service\Auth;

class Authorization
{
    // base
    const NONE          = 0b00000000;
    const READ_DATA     = 0b00000001;
    const WRITE_DATA    = 0b00000010;
    const READ_AUDIT    = 0b00000100;
    const WRITE_AUDIT   = 0b00001000;

    // composite
    const WRITE         = self::READ_DATA | self::WRITE_DATA;
    const AUDIT         = self::READ_DATA | self::READ_AUDIT | self::WRITE_AUDIT;
    const FULL          = self::WRITE | self::AUDIT;

    // shortcut/synonym
    const ALL           = self::FULL;
    const READ          = self::READ_DATA;

    public static function hasPermission(string $module, int $permission, array $available): bool
    {
        $result = false;
        foreach ($available as $mod => $perm) {
            if (fnmatch($mod, $module)) {
                if (($permission & $perm) == $permission) {
                    $result = true;
                } else {
                    $result = false;
                }
            }
        }

        return $result;
    }

    public static function isValidPermission($permission)
    {
        // empty is valid value, means no permission
        if (!$permission) {
            return true;
        }

        $modulePattern = '/[a-zA-Z\.\*]+\s*/';

        $lines = explode("\n", $permission);
        foreach ($lines as $line) {
            $line = trim($line);
            if (!$line) {
                continue;
            }

            $tokens = explode(':', $line);
            if ((count($tokens) != 2) || !preg_match($modulePattern, $tokens[0])) {
                return false;
            }

            $perms = trim($tokens[1]);
            if (!$perms) {
                return false;
            }

            $perms = explode(',', $perms);
            $class = self::class;
            $result = false;
            foreach ($perms as $perm) {
                $perm = strtoupper(trim($perm));
                $result = defined("$class::$perm");
            }
            return $result;
        }

        return true;
    }

    /**
     * check if $path is within $zone
     * @param $zone
     * @param $path
     * @return bool
     */
    public static function isWithinZone($zone, $path)
    {
        $zone = $zone . '.*';
        $path = $path . '.';

        return fnmatch($zone, $path);
    }

    public static function parsePermission(string $permission)
    {
        $className = self::class;
        $constName = strtoupper(trim($permission));

        return @constant("$className::$constName");
    }

    public static function deserializePermission($serialized)
    {
        $className = self::class;

        $result = [];

        if (!$serialized) {
            return $result;
        }

        $serialized = trim($serialized);
        if (!$serialized) {
            return $result;
        }

        $tokens = explode("\n", $serialized);
        foreach ($tokens as $token) {
            $token = trim($token);
            if (!$token) {
                continue;
            }

            list($key, $val) = explode(':', $token);
            $key = trim($key);

            $perms = [];
            $vals = explode(',', $val);
            foreach ($vals as $v) {
                $constName = strtoupper(trim($v));
                if (!$constName) {
                    continue;
                }

                $perms[] = constant("$className::$constName");
            }

            $result[$key] = array_reduce($perms, "$className::reducePermissionsCallback");
        }

        return $result;
    }

    private static function reducePermissionsCallback($previous, $current)
    {
        return ($previous === null) ? $current : $previous | $current;
    }
}
