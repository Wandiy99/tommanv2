<?php

namespace App\Service\Auth;

use Illuminate\Support\Facades\Cache;

class CredentialCache
{
    const KEY_PREFIX = 'Auth.Credential.Id::';

    private static function cache()
    {
        return Cache::store('session')->tags('Auth.Credential');
    }

    public static function store($login, $password)
    {
        return self::cache()->forever(self::KEY_PREFIX.$login, $password);
    }

    public static function retrieve($login)
    {
        return self::cache()->get(self::KEY_PREFIX.$login);
    }

    public static function isCached($login)
    {
        return self::cache()->has(self::KEY_PREFIX.$login);
    }
}
