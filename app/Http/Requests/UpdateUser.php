<?php

namespace App\Http\Requests;

use App\Service\Auth\Authorization;
use App\Service\SessionHelper;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('auth.user', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'workzone_id' => 'required',
            'permission_id' => 'required'
        ];
    }
}
