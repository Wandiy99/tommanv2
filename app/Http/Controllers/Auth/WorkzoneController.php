<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\SessionHelper;

class WorkzoneController extends Controller
{
    private $currentUser;
    private $hasWriteAccess;

    // TODO: decide on HTTP caching strategy
    public function index()
    {
        // TODO: refactor vue component for template caching
        return view('auth.workzone');
    }

    public function apiAll()
    {
        // TODO: 304 NOT MODIFIED
        [$result, $lastModified] = WorkzoneCached::all();

        // these fields are required by addWritableAttribute()
        $this->currentUser = SessionHelper::getCurrentUser();
        $this->hasWriteAccess = Authorization::hasPermission(
            'auth.workzone',
            Authorization::WRITE,
            $this->currentUser->permission
        );

        array_walk($result, [$this, 'addWritableAttribute']);

        return response()
            ->json($result)
            ->header('Last-Modified', $lastModified);
    }

    public function apiCreate(Request $request)
    {
        $label = $request->input('label');
        $path = $request->input('path', null);

        $currentUser = SessionHelper::getCurrentUser();
        if ($currentUser->workzone_path && !Authorization::isWithinZone($currentUser->workzone_path, $path)) {
            return abort(403);
        }

        $result = WorkzoneCached::create($label, $path);

        return response()->json($result);
    }

    public function apiUpdate(Request $request)
    {
        $id   = $request->input('id');
        $label = $request->input('label');

        $currentUser = SessionHelper::getCurrentUser();
        [$data, $lastModified] = WorkzoneCached::getById($id);
        if ($currentUser->workzone_path && !Authorization::isWithinZone($currentUser->workzone_path, $data->path)) {
            return abort(403);
        }

        $result = WorkzoneCached::update($id, $label);

        return response()->json($result);
    }

    private function addWritableAttribute($data)
    {
        if (!$this->hasWriteAccess) {
            return;
        }

        $zone = $this->currentUser->workzone_path;
        $path = $data->path;
        if (!$this->currentUser->workzone_id || Authorization::isWithinZone($zone, $path)) {
            $data->isWritable = true;
        }

        array_walk($data->children, [$this, 'addWritableAttribute']);
    }
}
