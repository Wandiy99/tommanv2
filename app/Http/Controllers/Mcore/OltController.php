<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Mcore\StoCached;
use App\Service\Mcore\StoRoomCached;

class OltController extends Controller
{
    public function createForm($stoId, $roomId)
    {
        [$stoData, $stomtime] = StoCached::getById($stoId);
        [$roomData, $roommtime] = StoRoomCached::getById($roomId);

        return view('mcore.olt.form', compact('stoData', 'roomData'));
    }

    public function create(Request $request, $stoId, $roomId)
    {
        // TODO
    }
}
