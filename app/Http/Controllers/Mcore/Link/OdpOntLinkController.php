<?php

namespace App\Http\Controllers\Mcore\Link;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\Link\SubmitOdpOnt;
use App\Service\SessionHelper;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\OdpCached;
use App\Service\Mcore\PelangganCached;
use App\Service\Mcore\Link\OdpToOntCached;

class OdpOntLinkController extends Controller
{
    public function odpForm($odpId, $odpPort)
    {
        [$linkData, $mtime] = OdpToOntCached::getByOdp($odpId, $odpPort);

        if (!$linkData) {
            [$odpData, $odp_mtime] = OdpCached::getById($odpId);

            $linkData = (object)[
                'src_id' => $odpId,
                'src_val' => $odpPort,

                'odp_id' => $odpData->id,
                'odp_label' => $odpData->label,
                'odp_workzone_id' => $odpData->workzone_id,
                'odp_workzone_label' => $odpData->workzone_label
            ];
        }

        $canEdit = true; // TODO

        return view(
            'mcore.link.odp-ont',
            compact('linkData', 'canEdit')
        );
    }

    public function pelangganByWorkzoneAsSelect2(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);

        [$pelangganPaged, $pelanggan_mtime] = OdpToOntCached::paginatePelangganByWorkzonePath(
            $workzoneData->path,
            $page,
            $search
        );

        $result = [];
        $view = View::make('mcore.link.partial.pelanggan');
        foreach ($pelangganPaged->all() as $pelangganData) {
            $pelangganData->disabled = !!($pelangganData->odp_id);
            $pelangganData->text = $view->with(compact('pelangganData'))->render();

            $result[] = $pelangganData;
        }

        return [
            'results' => $result,
            'paginate' => [
                'more' => $pelangganPaged->hasMorePages()
            ]
        ];
    }

    public function odpPortsAsSelect2($odpId)
    {
        [$links, $links_mtime] = OdpToOntCached::getByOdp($odpId);
        [$odpData, $odp_mtime] = OdpCached::getById($odpId);
        $ports = OdpCached::linksToPorts($odpData->capacity, $links->all());

        $results = [];
        $view = View::make('mcore.link.partial.odp-port');
        foreach ($ports as $portNum => $link) {
            $html = $view->with(compact('portNum', 'link'))->render();

            $port = (object)[
                'id' => $portNum,
                'portNum' => $portNum,
                'text' => $html,
                'disabled' => $link !== false,
                'link' => $link
            ];

            $results[] = $port;
        }

        return compact('results');
    }

    public function pelangganForm($pelangganId)
    {
        [$linkData, $mtime] = OdpToOntCached::getByPelanggan($pelangganId);
        if (!$linkData) {
            [$pelangganData, $pelanggan_mtime] = PelangganCached::getById($pelangganId);

            $linkData = (object)[
                'dst_id' => $pelangganId,

                'pelanggan_id' => $pelangganData->id,
                'pelanggan_kode' => $pelangganData->kode,
                'pelanggan_label' => $pelangganData->label,
                'pelanggan_workzone_id' => $pelangganData->workzone_id,
                'pelanggan_workzone_label' => $pelangganData->workzone_label,
            ];
        }

        $canEdit = true; // TODO

        return view(
            'mcore.link.odp-ont',
            compact('linkData', 'canEdit')
        );
    }

    public function submit(SubmitOdpOnt $request)
    {
        if ($request->action == 'save') {
            return $this->save($request);
        } else {
            return $this->remove($request);
        }
    }

    private function save($request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdpToOntCached::save($user->id, $request->src_id, $request->src_val, $request->med_val, $request->dst_id);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menyimpan data Sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menyimpan data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    private function remove($request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdpToOntCached::remove($user->id, $request->src_id, $request->src_val, $request->dst_id);

            $url = str_replace('/link/', '/', $request->path());
            if ($request->is('*/odp/*')) {
                $lastPos = strpos($url, '/port/');
                $url = substr($url, 0, $lastPos);
            }

            return redirect($url)->with('alerts', [
                [
                    'type' => 'warning',
                    'text' => '<strong>Sukses</strong> mencabut Sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menghapus data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
