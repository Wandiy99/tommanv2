<?php

namespace App\Http\Controllers\Mcore;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitStoRoom;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Mcore\StoCached;
use App\Service\Mcore\StoRoomCached;
use App\Service\Mcore\OdfCached;

class StoRoomController extends Controller
{
    public function create(SubmitStoRoom $request, $id)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            StoRoomCached::create($user->id, $id, $request->label);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Berhasil</strong> menambah data ruangan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function view($stoId, $roomId)
    {
        [$stoData, $sto_mtime] = StoCached::getById($stoId);
        [$roomData, $room_mtime] = StoRoomCached::getById($roomId);
        [$odfGroup, $odf_mtime] = OdfCached::listByRoomId($roomId);

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.sto', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $stoData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        return view('mcore.sto.room', compact('stoData', 'roomData', 'odfGroup', 'canEdit'));
    }

    public function update(SubmitStoRoom $request, $stoId, $roomId)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            StoRoomCached::update($user->id, $roomId, $request->label);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Berhasil</strong> merubah data'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function listOdfByRoomAsSelect2($roomId)
    {
        [$odfGroup, $odf_mtime] = OdfCached::listByRoomId($roomId);

        $result = [];
        foreach ($odfGroup as $group => $children) {
            foreach ($children as $room) {
                $room->text = $room->label;
            }

            $result[] = (object)[
                'text' => $group,
                'children' => $children
            ];
        }

        return $result;
    }
}
